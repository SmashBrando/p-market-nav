jQuery(document).ready(function() {
  $navigation = jQuery('#pentonNavigation');
  $navigation.prependTo(document.body);

  updateActiveLinks();
});

function updateActiveLinks() {
  pathArray = location.href.split('/');
  protocol = pathArray[0];
  host = pathArray[2];
  url = protocol + '//' + host;
  findHref = 'a[href^="' + url + '"]';
  jQuery('#pentonNavigation').find(findHref).addClass('active');
}

jQuery(document).ready(function() {
  var initialHeight = 43;
  $(".mk-header-holder").css({
    'marginTop': initialHeight + "px"
    //'paddingBottom': $scrollTop - h + "px"
  });
});

jQuery(window).scroll(function(){
    var h = 43;    
    var $scrollTop = $(window).scrollTop();


    if($scrollTop == 0){
      $(".mk-header-holder").css({
          'marginTop': h + "px"
          //'paddingBottom': $scrollTop - h + "px"
        });
    }

    if($scrollTop < 43 && $scrollTop > 0){
        $(".mk-header-holder").css({
          'marginTop': h - $scrollTop + "px"
          //'paddingBottom': $scrollTop - h + "px"
        });
    }
    
    //$('.mk-header-holder').text($scrollTop);
});